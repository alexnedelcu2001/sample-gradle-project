package com.example;

public class Main {
    public static void main(String[] args) {
        if (args.length != 3 && args.length != 2) {
            System.out.println("Error");
        }
        else if(args.length == 3){
            String operation = args[0];

            VerifyOperand.verifyNumber(args[1]);
            VerifyOperand.verifyNumber(args[2]);

            double operand1 = Double.parseDouble(args[1]);
            double operand2 = Double.parseDouble(args[2]);

            Calculator calculator = new Calculator();
            double result = calculator.performOperation(operation, operand1, operand2);
            System.out.println("Result: " + result);
        }
        else{
            String operation = args[0];
            if(operation.equals("sqrt") ){

                VerifyOperand.verifyNumber(args[1]);

                double operand1 = Double.parseDouble(args[1]);

                Calculator calculator = new Calculator();
                double result = calculator.performOperation(operation, operand1);
                System.out.println("Result: " + result);
            }
            else{
                System.out.println("Error");
            }
        }
    }
}