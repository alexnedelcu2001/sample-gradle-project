package com.example;

public class Calculator {
    public double performOperation(String operation, double a, double b) {
        switch (operation) {
            case "+":
                return Operations.add(a, b);
            case "-":
                return Operations.subtract(a, b);
            case "*":
                return Operations.multiply(a, b);
            case "/":
                return Operations.divide(a, b);
            case "min":
                return Operations.min(a, b);
            case "max":
                return Operations.max(a, b);
            default:
                throw new IllegalArgumentException("Invalid operation: " + operation);
        }
    }

    public double performOperation(String operation, double a) {
        switch (operation) {
            case "sqrt":
                return Operations.sqrt(a);
            default:
                throw new IllegalArgumentException("Invalid operation: " + operation);
        }
    }
}
