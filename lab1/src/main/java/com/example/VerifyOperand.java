package com.example;

public class VerifyOperand {

    public static void verifyNumber(String operand) {

        try {
            double number;
            number = Double.parseDouble(operand);

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid operand: " + operand);
        }

    }

}

