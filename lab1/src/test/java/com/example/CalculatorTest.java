package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void performOperation() {
        Calculator calculator = new Calculator();
        double result = calculator.performOperation("+", 5.0, 3.0);
        assertEquals(8.0, result, 0.01);
    }

    @Test
    void testPerformOperation() {
        Calculator calculator = new Calculator();
        double result = calculator.performOperation("sqrt", 25.0);
        assertEquals(5.0, result, 0.01);
    }

    @Test
    void performOperation2() {
        Calculator calculator = new Calculator();

        try {
            calculator.performOperation("invalid", 5.0, 3.0);

        } catch (IllegalArgumentException e) {

            assertEquals("Invalid operation: invalid", e.getMessage());
        }
    }

    @Test
    void testPerformOperation2() {
        Calculator calculator = new Calculator();

        try {
            calculator.performOperation("sqr", -1.0);

        } catch (IllegalArgumentException e) {

            assertEquals("Invalid operation: sqr", e.getMessage());
        }
    }
}