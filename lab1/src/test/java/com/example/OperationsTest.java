package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {

    @Test
    void add() {
        assertEquals(4.0, Operations.add(2.0, 2.0));
        assertEquals(0.0, Operations.add(-2.0, 2.0));
        assertEquals(-2.0, Operations.add(0.0, -2.0));
        assertEquals(7.5, Operations.add(5.5, 2.0));
        assertEquals(0.0, Operations.add(0.0, 0.0));
    }

    @Test
    void subtract() {
        assertEquals(0.0, Operations.subtract(2.0, 2.0));
        assertEquals(-4.0, Operations.subtract(-2.0, 2.0));
        assertEquals(2.0, Operations.subtract(0.0, -2.0));
        assertEquals(3.5, Operations.subtract(5.5, 2.0));
        assertEquals(0.0, Operations.subtract(0.0, 0.0));

    }

    @Test
    void multiply() {
        assertEquals(4.0, Operations.multiply(2.0, 2.0));
        assertEquals(-4.0, Operations.multiply(-2.0, 2.0));
        assertEquals(0.0, Operations.multiply(0.0, 2.0));
        assertEquals(11.0, Operations.multiply(5.5, 2.0));
        assertEquals(0.0, Operations.multiply(0.0, 0.0));

    }

    @Test
    void divide() {
        assertEquals(1.0, Operations.divide(2.0, 2.0));
        assertEquals(-1.0, Operations.divide(-2.0, 2.0));
        assertEquals(0.0, Operations.divide(0.0, 5.5));
        assertEquals(2.75, Operations.divide(5.5, 2.0));
        assertThrows(ArithmeticException.class, () -> Operations.divide(5.5, 0.0));

    }

    @Test
    void min() {
        assertEquals(2.0, Operations.min(2.0, 2.0));
        assertEquals(-2.0, Operations.min(-2.0, 2.0));
        assertEquals(-2.0, Operations.min(0.0, -2.0));
        assertEquals(2.0, Operations.min(5.5, 2.0));
        assertEquals(0.0, Operations.min(0.0, 0.0));

    }

    @Test
    void max() {
        assertEquals(2.0, Operations.max(2.0, 2.0));
        assertEquals(2.0, Operations.max(-2.0, 2.0));
        assertEquals(0.0, Operations.max(0.0, -2.0));
        assertEquals(5.5, Operations.max(5.5, 2.0));
        assertEquals(0.0, Operations.max(0.0, 0.0));

    }

    @Test
    void sqrt() {
        assertEquals(2.0, Operations.sqrt(4.0));
        assertEquals(0.0, Operations.sqrt(0.0));
        assertThrows(ArithmeticException.class, () -> Operations.sqrt(-1.1));
    }
}