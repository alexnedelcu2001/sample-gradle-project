package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerifyOperandTest {

    @Test
    void verifyNumberInvalid() {
        String invalidOperand = "abc";
        assertThrows(IllegalArgumentException.class, () -> VerifyOperand.verifyNumber(invalidOperand));
    }
}