package org.example;

import java.util.Set;
import java.util.TreeSet;

//arbore binar echilibrat
public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSet<>();
    }

    // O(logn)
    @Override
    public void add(T item) {
        set.add(item);
    }

    // O(logn)
    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    // O(logn)
    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
