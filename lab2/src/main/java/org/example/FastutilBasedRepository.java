package org.example;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;

public class FastutilBasedRepository<T> implements InMemoryRepository<T> {
    private ObjectSet<T> set;

    public FastutilBasedRepository() {
        this.set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
