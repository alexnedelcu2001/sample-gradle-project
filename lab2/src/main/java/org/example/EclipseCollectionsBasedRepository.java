package org.example;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;

public class EclipseCollectionsBasedRepository<T> implements InMemoryRepository<T> {
    private MutableSet<T> set;

    public EclipseCollectionsBasedRepository() {
        this.set = Sets.mutable.empty();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}


