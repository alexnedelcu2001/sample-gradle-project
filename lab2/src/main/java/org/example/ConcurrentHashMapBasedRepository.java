package org.example;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private ConcurrentHashMap<T, Boolean> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        map.put(item, Boolean.TRUE);
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }

    @Override
    public void remove(T item) {
        map.remove(item);
    }
}
