package benchmark;

import org.example.ArrayListBasedRepository;
import org.example.EclipseCollectionsBasedRepository;
import org.example.Order;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)

@State(Scope.Benchmark)
public class EclipseCollectionsBasedRepositoryBenchmark {
    private EclipseCollectionsBasedRepository<Order> repository;
    private Order order;

    @Setup
    public void setup() {
        repository = new EclipseCollectionsBasedRepository<>();
        order = new Order(1,20,300);
    }

    @Benchmark
    public void benchmarkAdd() {
        repository.add(order);
    }

    @Benchmark
    public void benchmarkContains() {
        repository.contains(order);
    }

    @Benchmark
    public void benchmarkRemove() {
        repository.remove(order);
    }

}
